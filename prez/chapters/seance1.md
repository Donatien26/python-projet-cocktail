﻿## Séance 1





### Objectif du projet
- Produire une application
- ⚠️ Pas d'interface graphique
- Privilégier l'ajout de fonctionnalitées






### Présentation
- Proposer un moteur de recherche de cocktails en se basant sur l'api https://www.thecocktaildb.com/api.php.







### Fonctionnalités minimale
- Effectuer des appels à l’API afin de récupérer des cocktails en fonction de leur nom, ou d’ingrédients.
- Pouvoir ajouter des recettes de cocktails (base de données).
- Donner des avis sur des cocktails.
- Gestion de favoris (ajout et suppression).
- Gestion d’historique.
- Une gestion de compte applicatif (non connecté, connecté, admin)







### Fonctionnalités avancées
- L’API dans sa version gratuite ne prend en charge que les recherches avec un seul paramètre. Implémentez à votre manière une recherche à plusieurs paramètres.
- Traduire les descriptions des cocktails en français (utilisation d’une deuxième API, et oui vive l’architecture micro service :) ).
- Affichage de l’image du cocktail.







### Objectif des séances
- 3 séances pour de la modélisation
- Travail en autonomie
- produire le rapport intermédiaire







### Contenu du rapport intermédiaire
- Diagramme de Gant (Plannification)
- Diagramme de cas d'utilisation
- Diagramme d'activité
- Diagramme de base de donnée
- 1 ou 2 diagrammes de séquences
- Diagramme de packages
- Autre diagrammes
- Eventuellement comment vous imaginez vos écrans s'enchainer







### Petits conseils
- N'hésitez pas à venir me voir
- Définissez un chef de projet
-  ??????