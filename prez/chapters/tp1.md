## TP1







### A retenir
Le code MINIMAL pour créer un objet 

```python
class Personnage:

    """
    Un constructeur est la méthode classique pour initialiser un objet
    """
    def __init__(self, attribut1):
        self.__attribut1=attribut1

    """
    Un getter permet d'acceder aux attributs d'une classe. 
    """
    @property
    def attribut1(self):
        return self.__attribut1

    """
    Un setter permet de modifier un attributs d'un constructeur
    """

    @attribut1.setter
    def attribut1(self,attribut1):
        self.__attribut1=attribut1

    def uneMethod():
        print "toto"

```







### Remarque
- On peut faire sans getter et setter. Perso je trouve ca moins beau ^^

```python
class Personnage:

    """
    Un constructeur est la méthode classique pour initialiser un objet
    """
    def __init__(self, attribut1):
        self.attribut1=attribut1 # On note l'abscence du double tiret ou du simple tiret qui va permettre d'acceder a l'attribut attribut1 sans definir des getter et setter (implicite)

    def uneMethod():
        print "toto"

```







### Les classes abstraites
- eviter la redondance de codes
- un template de classe utilisé par les classes filles (classe qui hérite)

```python
class Personnage(ABC):

    """
    Y a aussi un constructeur
    """
    def __init__(self, attribut1):
        self.__attribut1=attribut1

    """
    Y a aussi des getter et setter
    """
    @property
    def attribut1(self):
        return self.__attribut1

    @attribut1.setter
    def attribut1(self,attribut1):
        self.__attribut1=attribut1

    @abstractmethod
    def methodAbstraite(self,attribut1):
        """
        Ne pas mettre de code, il est dans les classes filles
        """
```


## Les classes services
- Ce n'est pas des classes qui décrivent un objet donc pas de constructeur
- annotation @staticmethod au dessus des méthodes de la classe

```python
class PersonnageService:

    @staticmethod
    def attaquer(personnage):
        """fait des trucs"""
```



## A retenir
- Programmation orienté objets = forte cohérence (au sein d'une classe) faible couplage (entre les classes)







### Git
Les commandes qu'on a utilisées:
- git clone .... (copié un repertoire)
- git checkout .... (changé de branche ou de tag)
- git pull --all (récupèrer tout le code dispo sur le repertoire distant)
- git add . + git commit -m "..." (sauvegarder mon code dans git)
- git status (quand on panique !!!)
- git reset --hard (si je veux annulé tout ce que j'ai fait !!! irreversible !!!)

